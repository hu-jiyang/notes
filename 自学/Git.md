# Git
![Alt text](./Git_img/git%E6%B5%81%E7%A8%8B%E5%9B%BE.drawio.png)
## 基本配置
配置用户名
- git config --global user.name "用户名"

配置邮箱
- git config --global user.email "邮箱"
### 配置命令别名
touch ~/.bashrc:在用户根目录创建.bashrc文件
配置如下内容:

```
#格式化输出当前目录的内容
alias ll='ls -al'
#格式化输出git日志
alias git-log='git log --pretty=oneline --all --graph --abbrev-commit'
#创建并切换分支
alias checkoutb='git checkout -b'
```
## 基本指令
创建新目录
- mkdir

创建新文件
- touch

查看目录内容
- ls/ll

查看文件内容
- cat

查看修改状态
- git status

添加到暂存区(.为通配符)
- git add .

暂存区提交到库
- git commit -m "备注"

回滚:
- git reset --hard 记录ID

输出操作日志
- git log/git-log
- git -reflog
## 添加文件到忽略列表
在工作区创建.gitignore文件,在文件中选择不提交的文件或者文件类型
## 分支
### 查看分支
- git branch
### 创建分支
- git branch 分支名
### 切换分支
- git checkout 分支名
### 创建并切换分支
- git checkout -b 分支名
### 合并分支
- git merge 分支名
### 删除分支
- git branch -d 分支名
- git branch -D 分支名
### 解决冲突
### 开发中的分支使用原则
- master(生产分支)
- develop(开发分支)
- future/xxxx分支(从develop创建的分支)
- hotfix/xxxx分支(从master创建的分支)
## 远程仓库
### 添加远程仓库
- git remote add origin 仓库路径
### 查看远程仓库
- git remote
### 推送到远程仓库
- git push [-f] [--set-upstream] origin master [本地分支名:远端分支名] //-f为强推
### 查看本地分支与远程仓库的关联
- git branch -vv
### 克隆远程仓库
- git clone 仓库路径 [本地路径]
### 抓取远程仓库
- git fetch [远程仓库名称] [远程分支]
### 拉取远程仓库
- git pull[远程仓库名称] [远程分支]